/*==== MENU SHOW ====*/

const showMenu = (toggleID, navId) => {
	const toggle = document.getElementById(toggleID);
		  nav = document.getElementById(navId);

	if(toggle && nav) {
		toggle.addEventListener("click", ()=>{
			nav.classList.toggle("show");
		});
	}
}

showMenu("nav-toggle","nav-menu");

/*==== Remove menu mobile ====*/

const navLink = document.querySelectorAll(".nav_link");

navLink.forEach(n => n.addEventListener("click",linkAction));

function linkAction() {
	navLink.forEach(n => n.classList.remove("active"));
	this.classList.add("active");

	//Remolve mobile menu
	const navMenu = document.getElementById("nav-menu");
	navMenu.classList.remove("show");
}